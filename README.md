## SEU Tester

This Python script simulates **Single Event Upsets** (random bit flips caused due to
radiation) in the binary of an executable program.

![Animated SEU Tester Demo](docs/demo.gif)

### Usage

1. Prepare an executable file called `a.out`. For example, to use the bundled Fibonacci
   program:
   ```bash
   gcc fibonacci.c
   ```
2. Install Python 3 requirements:
   ```bash
   pip install -r requirements.txt
   ```
3. Run the script:
   ```bash
   python tester.py
   ```

_Note_: Running modified programs may lead to weird, inconsistent behaviour, infinite
loops, crashes, memory overallocation, or loss of data. Handle with care!

### Terminology

![SEU Tester Output](docs/screenshot.png)

- **Correct**: Binary returned correct output
- **Incomplete**: Binary returned only part of the output
- **Incorrect**: Program returned output unequal to the correct one
- **Empty**: Program didn't return anything
- **Crash**: Program detected an exception or returned with a non-zero code
- **No crash**: Program didn't return any errors (regardless of correct/incorrect output)

The **no crash**/**incorrect** combination is the most interesting to see, as it
represents your program outputting wrong data, without any way to detect that it's
incorrect!
