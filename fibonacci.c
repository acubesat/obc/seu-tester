#include <stdio.h>
int main() {
    int i, n = 100;
    unsigned long long int t1 = 0, t2 = 1, nextTerm;
    printf("Fibonacci Series: ");

    for (i = 1; i <= n; ++i) {
        printf("Fibonacci %d: %llu\n", i, t1);
        nextTerm = t1 + t2;
        t1 = t2;
        t2 = nextTerm;
    }

    return 0;
}

