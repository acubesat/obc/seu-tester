#!/usr/bin/env python3

import random
import copy
import subprocess
import os
from tqdm import tqdm

# import subprocess
# process = subprocess.Popen(['echo', 'More output'],
#                      stdout=subprocess.PIPE,
#                      stderr=subprocess.PIPE)
# stdout, stderr = process.communicate()
# stdout, stderr


def bitFlip(byte):
    """Performs a random bit flip on a byte"""
    return byte ^ (1 << random.randint(0,7))

with open('a.out', 'rb') as file:
    data = bytearray(file.read())

# Run the original file initially to get its output
process = subprocess.Popen(['./a.out'], stdout=subprocess.PIPE)
target, _ = process.communicate()
with open('tester.output.target', 'wb') as file:
    file.write(target)

resultsDummy = { 'success': 0, 'crash': 0 }
results = {
    'correct': copy.copy(resultsDummy),
    'incomplete': copy.copy(resultsDummy),
    'incorrect': copy.copy(resultsDummy),
    'empty': copy.copy(resultsDummy),
}

open('b.out', 'wb').close()
os.chmod('b.out', 0o770)

for i in tqdm(range(1000)):
    changedData = copy.deepcopy(data)
    randomByte = random.randint(0, len(data) - 1)
    changedData[randomByte] = bitFlip(changedData[randomByte])

    with open('b.out', 'wb') as file:
        file.write(changedData)

    try:
        process = subprocess.Popen(['./b.out'], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
        output, _ = process.communicate()
        returnCode = process.returncode
    except:
        output = str.encode('')
        returnCode = -1

    if output == target:
        result0 = 'correct'
    elif len(output) <= 1:
        result0 = 'empty'
    elif target.startswith(output):
        result0 = 'incomplete'
    else:
        result0 = 'incorrect'

    if returnCode == 0:
        result1 = 'success'
    else:
        result1 = 'crash'

    # if target != output:
    #     with open('tester.output.' + str(i), 'wb') as file:
    #         file.write(str.encode(str(returnCode)))
    #         file.write(str.encode("\n\n"))
    #         file.write(str.encode(result0 + " " + result1))
    #         file.write(str.encode("\n\n"))
    #         file.write(output)

    results[result0][result1] += 1

print(results)

import json
from pygments import highlight, lexers, formatters

formatted_json = json.dumps(results, sort_keys=True, indent=4)

colorful_json = highlight(formatted_json, lexers.JsonLexer(), formatters.TerminalFormatter())
print(colorful_json)
